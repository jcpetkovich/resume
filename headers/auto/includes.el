(TeX-add-style-hook
 "includes"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("geometry" "scale=0.8")))
   (TeX-run-style-hooks
    "inputenc"
    "fontawesome"
    "fontspec"
    "tabularx"
    "ragged2e"
    "geometry"
    "multicol"
    "import"))
 :latex)

