# resume - my resume
# See LICENSE file for copyright and license details.

include config.mk

SRC = resume.tex

all: ic-resume.pdf em-resume.pdf

%.pdf: %.tex subsections/*.tex headers/*.tex
	@echo CC $<
	@${CC} $<

${OBJ}: config.h config.mk

clean:
	@echo cleaning
	@rm -f *.pdf *.log *.out *.aux
	@rm -rf auto

.PHONY: all clean
